import { Test, TestingModule } from '@nestjs/testing';
import { VoucherCodeService } from 'src/services/voucher-code.service';
import { EntityManager } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SpecialOffer } from 'src/entities/special-offer.entity';
import { VoucherCode } from 'src/entities/voucher-code.entity';
import { Customer } from 'src/entities/customer.entity';
import { CustomerService } from 'src/services/customer.service';

describe('VoucherCodeService', () => {
  let voucherCodeService: VoucherCodeService;

  const mockRepository = {
    save: jest.fn(),
    findOne: jest.fn(),
    find: jest.fn(),
    createQueryBuilder: jest.fn(() => ({
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      innerJoin: jest.fn().mockReturnThis(),
      setParameter: jest.fn().mockReturnThis(),
      leftJoinAndSelect: jest.fn().mockReturnThis(),
      getOne: jest.fn().mockReturnThis(),
      getMany: jest.fn().mockReturnThis(),
    })),
  };

  const mockEntityManager = {
    transaction: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CustomerService,
        VoucherCodeService,
        {
          provide: getRepositoryToken(VoucherCode),
          useValue: mockRepository,
        },
        {
          provide: getRepositoryToken(Customer),
          useValue: mockRepository,
        },
        {
          provide: getRepositoryToken(SpecialOffer),
          useValue: mockRepository,
        },
        {
          provide: EntityManager,
          useValue: mockEntityManager,
        },
      ],
    }).compile();

    voucherCodeService = module.get<VoucherCodeService>(VoucherCodeService);
  });

  it('should be defined', () => {
    expect(voucherCodeService).toBeDefined();
  });

  describe('generateVoucherCodes', () => {
    it('should generate voucher codes', async () => {
      const specialOffer = new SpecialOffer();
      const customer = new Customer();
      const expirationDate = new Date();

      const voucherCode = new VoucherCode();
      voucherCode.code = 'GeneratedCode123';

      mockRepository.save.mockResolvedValue(voucherCode);

      const result = await voucherCodeService.generateVoucherCodes(specialOffer, customer, expirationDate);
      expect(result).toEqual(voucherCode);
      expect(mockRepository.save).toHaveBeenCalledWith({
        specialOffer,
        customer,
        expirationDate,
        code: 'GeneratedCode123',
      });
    });
  });

  describe('getVoucherCodesByEmail', () => {
    it('should get voucher codes by email', async () => {
      const email = 'test@example.com';
      const voucherCodes = ["test"];

      mockRepository.find.mockResolvedValue(voucherCodes);

      const result = await voucherCodeService.getVoucherCodesByEmail(email);
      expect(result).toEqual(voucherCodes);
      expect(mockRepository.find).toHaveBeenCalledWith({ where: { customer: { email } } });
    });
  });

  describe('validateAndUseVoucher', () => {
    it('should return null for an already used voucher code', async () => {
      const email = 'test@example.com';
      const voucherCode = new VoucherCode();
      voucherCode.usageDate = new Date();

      mockRepository.findOne.mockResolvedValue(voucherCode);

      const result = await voucherCodeService.validateAndUseVoucher('voucher-code', email);
      expect(result).toBeNull();
      expect(mockRepository.findOne).toHaveBeenCalledWith({ where: { code: 'voucher-code', customer: { email } } });
    });

    it('should return null for an invalid voucher code', async () => {
      const email = 'test@example.com';

      mockRepository.findOne.mockResolvedValue(null);

      const result = await voucherCodeService.validateAndUseVoucher('invalid-code', email);
      expect(result).toBeNull();
      expect(mockRepository.findOne).toHaveBeenCalledWith({ where: { code: 'invalid-code', customer: { email } } });
    });
  });
});