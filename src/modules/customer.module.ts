import { Module } from '@nestjs/common';
import  { CustomerController } from 'src/controllers/customer.controller';
import { CustomerService } from 'src/services/customer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from 'src/entities/customer.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Customer])
  ],
  controllers: [CustomerController],
  providers: [CustomerService],
  exports: [CustomerService]
})
export class CustomerModule {}