import { Module } from '@nestjs/common';
import { SpecialOfferController } from 'src/controllers/special-offer.controller';
import { SpecialOfferService } from 'src/services/special-offer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpecialOffer } from 'src/entities/special-offer.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([SpecialOffer])
  ],
  controllers: [SpecialOfferController],
  providers: [SpecialOfferService],
  exports: []
})
export class SpecialOfferModule {}