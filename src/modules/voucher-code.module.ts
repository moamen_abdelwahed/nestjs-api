import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoucherCodeController } from 'src/controllers/voucher-code.controller';
import { VoucherCodeService } from 'src/services/voucher-code.service';
import { CustomerModule } from './customer.module';
import { VoucherCode } from 'src/entities/voucher-code.entity';
import { Customer } from 'src/entities/customer.entity';
import { SpecialOffer } from 'src/entities/special-offer.entity';

@Module({
  imports: [
    CustomerModule,
    TypeOrmModule.forFeature([VoucherCode, Customer, SpecialOffer])
  ],
  controllers: [VoucherCodeController],
  providers: [VoucherCodeService],
  exports: [VoucherCodeService]
})
export class VoucherModule {}