import { Controller, Post, Body, Get, Query } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { VoucherCodeService } from 'src/services/voucher-code.service';
import { SpecialOffer } from 'src/entities/special-offer.entity';
import { Customer } from 'src/entities/customer.entity';

@ApiTags('vouchers')
@Controller('api/vouchers')
export class VoucherCodeController {
  constructor(private readonly voucherCodeService: VoucherCodeService) {}

  @Post('generate')
  @ApiQuery({ name: 'specialOfferId', required: true, description: 'ID of the special offer' })
  @ApiQuery({ name: 'customerId', required: true, description: 'ID of the customer' })
  @ApiQuery({ name: 'expirationDate', required: true, description: 'Expiration date in ISO format (e.g., "2023-12-31T23:59:59.999Z")' })
  async generateVoucherCodes(
    @Body() body: { specialOfferId: number; customerId: number; expirationDate: string },
  ) {
    const specialOffer = new SpecialOffer();
    specialOffer.id = body.specialOfferId;

    const customer = new Customer();
    customer.id = body.customerId;

    try {
      const voucherCodes = await this.voucherCodeService.generateVoucherCodes(
        specialOffer,
        customer,
        new Date(body.expirationDate),
      );

      return { success: true, data: voucherCodes };
    } catch (error) {
      return { success: false, message: 'Failed to generate voucher codes' };
    }
  }

  @Get('by-email')
  async getVoucherCodesByEmail(@Query('email') email: string) {
    return this.voucherCodeService.getVoucherCodesByEmail(email);
  }

  @Post('use')
  @ApiQuery({ name: 'voucherCode', required: true, description: 'Voucher Code' })
  @ApiQuery({ name: 'email', required: true, description: 'Customer Email' })
  async validateVoucher(@Body() requestBody: { voucherCode: string, email: string }) {
    const { voucherCode, email } = requestBody;

    const percentageDiscount = await this.voucherCodeService.validateAndUseVoucher(email, voucherCode);

    if (percentageDiscount !== null) {
      return { percentageDiscount, message: 'Voucher validated and used successfully.' };
    } else {
      return { message: 'Invalid voucher code.' };
    }
  }
}
