import { Module } from "@nestjs/common";
import { CustomerService } from "./customer.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Customer } from "src/entities/customer.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([Customer])
    ],
    providers: [CustomerService],
})
export class RootTestModule {}