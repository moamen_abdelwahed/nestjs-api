import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VoucherCode } from 'src/entities/voucher-code.entity';
import { SpecialOffer } from 'src/entities/special-offer.entity';
import { Customer } from 'src/entities/customer.entity';
import { CustomerService } from 'src/services/customer.service';
import { EntityManager, Repository } from 'typeorm';
import shortid from 'shortid';

@Injectable()
export class VoucherCodeService {

  constructor(
    @InjectRepository(VoucherCode)
    private readonly voucherCodeRepository: Repository<VoucherCode>,
    private readonly customerService: CustomerService,
    private readonly entityManager: EntityManager

  ) {}

  async generateVoucherCodes(
    specialOffer: SpecialOffer,
    customer: Customer,
    expirationDate: Date,
  ): Promise<VoucherCode> {
    return this.entityManager.transaction(async transactionalEntityManager => {
      const voucherCode = new VoucherCode();
      voucherCode.specialOffer = specialOffer;
      voucherCode.customer = customer;
      voucherCode.expirationDate = expirationDate;
      voucherCode.isUsed = false;
      voucherCode.code = shortid.generate();
  
      await transactionalEntityManager.save(VoucherCode, voucherCode);
  
      return voucherCode;
    });
  }

  async getVoucherCodesByEmail(email: string) {
    return this.voucherCodeRepository.createQueryBuilder('voucher_code')
    .innerJoin('voucher_code.customer', 'customer')
    .where('customer.email = :email', { email })
    .getMany();
  }

  async validateAndUseVoucher(email: string, voucherCode: string): Promise<number | null> {
    const customer = await this.customerService.findByEmail(email);

    if (!customer) {
      return null;
    }

    const voucher = await this.voucherCodeRepository.createQueryBuilder('voucher_code')
    .innerJoinAndSelect('voucher_code.specialOffer', 'special_offer')
    .where('voucher_code.code = :code', { code: voucherCode })
    .andWhere('voucher_code.isUsed = :isUsed', { isUsed: 0 })
    .andWhere('voucher_code.expirationDate > NOW()')
    .getOne();

    if (!voucher) {
      return null;
    }

    voucher.isUsed = true;
    voucher.usageDate = new Date();
    await this.voucherCodeRepository.save(voucher);

    return voucher.specialOffer.fixedPercentageDiscount;
  }
}