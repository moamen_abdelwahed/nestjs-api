import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/entities/customer.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CustomerService {
    constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    ) {}

    async findByEmail(email: string): Promise<Customer | null> {
        return this.customerRepository.findOne({ where: { email } });
    }
}