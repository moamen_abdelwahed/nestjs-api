import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { VoucherCode } from 'src/entities/voucher-code.entity';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @OneToMany(() => VoucherCode, (voucherCode) => voucherCode.customer)
  voucherCodes: VoucherCode[];
}
