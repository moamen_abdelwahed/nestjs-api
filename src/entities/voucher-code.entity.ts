import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Customer } from 'src/entities/customer.entity';
import { SpecialOffer } from 'src/entities/special-offer.entity';

@Entity()
export class VoucherCode {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  code: string;

  @ManyToOne(() => Customer, (customer) => customer.voucherCodes)
  customer: Customer;

  @ManyToOne(() => SpecialOffer, (specialOffer) => specialOffer.voucherCodes)
  specialOffer: SpecialOffer;

  @Column({ nullable: true })
  usageDate: Date;
  
  @Column({ type: 'timestamp'})
  expirationDate: Date;
  
  @Column()
  isUsed: Boolean;
}
