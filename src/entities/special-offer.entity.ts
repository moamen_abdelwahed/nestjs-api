import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { VoucherCode } from 'src/entities/voucher-code.entity';

@Entity()
export class SpecialOffer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  fixedPercentageDiscount: number;

  @OneToMany(() => VoucherCode, (voucherCode) => voucherCode.specialOffer)
  voucherCodes: VoucherCode[];
}