import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './modules/customer.module';
import { SpecialOfferModule  } from './modules/special-offer.module';
import { VoucherModule  } from './modules/voucher-code.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { SpecialOffer } from './entities/special-offer.entity';
import { VoucherCode } from './entities/voucher-code.entity';
import { ConfigModule } from '@nestjs/config';
import { RateLimiterAppModule } from './modules/rate-limiter.module';
import { VoucherCodeController } from './controllers/voucher-code.controller';

@Module({
  imports: [
    RateLimiterAppModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST,
      port: 3306,
      username: process.env.MYSQL_USER,
      database: process.env.MYSQL_DB,
      entities: [Customer, SpecialOffer, VoucherCode],
      migrations: ["dist/migrations/*.js"],
      synchronize: true,
      logging: true
    }),
    CustomerModule,
    SpecialOfferModule,
    VoucherModule
  ],
  controllers: [AppController, VoucherCodeController],
  providers: [AppService],
})
export class AppModule {}
