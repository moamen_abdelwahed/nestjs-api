import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

export class CreateVoucherCodeTable1692605898538 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
              name: 'voucher_code',
              columns: [
                {
                  name: 'id',
                  type: 'int',
                  isPrimary: true,
                  isGenerated: true,
                  generationStrategy: 'increment',
                },
                {
                  name: 'code',
                  type: 'varchar',
                  length: '255',
                  isUnique: true,
                },
                {
                  name: 'expiration_date',
                  type: 'timestamp',
                },
                {
                  name: 'used',
                  type: 'boolean',
                  default: false,
                },
                {
                  name: 'customer_id',
                  type: 'int',
                },
                {
                  name: 'special_offer_id',
                  type: 'int',
                },
              ],
            }),
            true,
        );
        
        await queryRunner.createForeignKey(
            'voucher_code',
            new TableForeignKey({
                columnNames: ['customer_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'customer',
                onDelete: 'CASCADE',
            }),
        );
      
        await queryRunner.createForeignKey(
            'voucher_code',
            new TableForeignKey({
              columnNames: ['special_offer_id'],
              referencedColumnNames: ['id'],
              referencedTableName: 'special_offer',
              onDelete: 'CASCADE',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const voucherCodeTable = await queryRunner.getTable('voucher_code');
        if (voucherCodeTable) {
            const customerForeignKey = voucherCodeTable.foreignKeys.find(
                (fk) => fk.columnNames.indexOf('customer_id') !== -1,
            );
            const specialOfferForeignKey = voucherCodeTable.foreignKeys.find(
                (fk) => fk.columnNames.indexOf('special_offer_id') !== -1,
            );
            if (customerForeignKey) {
                await queryRunner.dropForeignKey('voucher_code', customerForeignKey);
            }
            if (specialOfferForeignKey) {
                await queryRunner.dropForeignKey('voucher_code', specialOfferForeignKey);
            }
        }
        await queryRunner.dropTable('voucher_code');
    }
}
