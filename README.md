# Voucher Pool API


This is a RESTful API for managing a voucher pool, allowing customers to use unique voucher codes for discounts on special offers.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Configuration](#configuration)
- [Usage](#usage)
  - [Generate Voucher Codes](#generate-voucher-codes)
  - [Validate and Use Voucher](#validate-and-use-voucher)
  - [Get Voucher Codes by Email](#get-voucher-codes-by-email)
- [Testing](#testing)
- [Rate Limiting](#rate-limiting)
- [Database Transactions](#database-transactions)
- [Endpoints](#endpoints)


## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js and npm installed
- MySQL database
- Git (optional)

## Getting Started

### Installation

To install and run this API locally, follow these steps:

1. Clone this repository (or download the ZIP file):

  ```bash
  git clone https://github.com/your-username/voucher-pool-api.git
  ```
2. Change to the project directory:

 ```bash
 cd nestjs-api
 ```
3.  Install dependencies:
 ```bash
 npm install
 ```

### Configuration

1. Create a `.env` file as a copy from `.env.example` and edit it with your own Database Credentails

2. Run database migrations:
 ```bash
 npm run migration:run
 ```

3. Start the server:
 ```bash
 npm run test
 ```

## Usage

### Generate Voucher Codes

To generate voucher codes for a customer and special offer, make a POST request to:
 ```bash
 POST /api/vouchers
 ```

Request Body:
```json
{
  "email": "customer@example.com",
  "specialOfferName": "SummerSale",
  "expirationDate": "2023-12-31"
}
```

### Validate and Use Voucher

To validate and use a voucher code, make a POST request to the following endpoint:
```http
POST /api/vouchers/use
```

Request Body:
```json
{
  "voucherCode": "your-voucher-code",
  "email": "customer@example.com"
}
```

### Get Voucher Codes by Email

To retrieve all valid voucher codes for a customer by email, make a GET request to the following endpoint:

```http
GET /api/vouchers?email=customer@example.com
```

## Testing

You can run unit tests using the following command:
```shell
npm run test
```

## Rate Limiting

This API implements rate limiting to protect against abuse. It enforces a maximum number of requests per minute for each endpoint.

## Endpoints

`POST /api/vouchers`: Generate voucher codes.
`POST /api/vouchers/validate`: Validate and use a voucher code.
`GET /api/vouchers`: Get valid voucher codes by email.